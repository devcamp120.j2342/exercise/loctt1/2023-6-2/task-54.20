package com.devcamp.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class testcampain {
     @CrossOrigin
     @GetMapping("/devcamp-simple")
     public String simple(){
          return "test campaign";
     }
}
